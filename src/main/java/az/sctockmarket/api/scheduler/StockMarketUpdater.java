package az.sctockmarket.api.scheduler;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class StockMarketUpdater {

  private final StockUpdater stockUpdater;
  private final OrderUpdater orderUpdater;

  @Transactional
  @Scheduled(fixedRateString = "PT10S")
  public void update() {
    stockUpdater.updatePrices();
    orderUpdater.updateFilledPrices();
  }
}

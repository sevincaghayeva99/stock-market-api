package az.sctockmarket.api.scheduler;

import az.sctockmarket.api.domain.entities.Order;
import az.sctockmarket.api.domain.entities.StockProduct;
import az.sctockmarket.api.domain.entities.Transaction;
import az.sctockmarket.api.domain.entities.User;
import az.sctockmarket.api.domain.enums.OrderType;
import az.sctockmarket.api.domain.enums.TransactionReason;
import az.sctockmarket.api.domain.enums.TransactionType;
import az.sctockmarket.api.domain.internal.MailMetaData;
import az.sctockmarket.api.domain.internal.MailTemplate;
import az.sctockmarket.api.repository.OrderRepository;
import az.sctockmarket.api.repository.StockProductRepository;
import az.sctockmarket.api.repository.TransactionRepository;
import az.sctockmarket.api.service.MailSender;
import az.sctockmarket.api.service.strategy.OrderValidatorFactory;
import az.sctockmarket.api.web.rest.dto.OrderDto;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderUpdater {

  private final OrderRepository orderRepository;
  private final StockProductRepository stockProductRepository;
  private final OrderValidatorFactory orderValidatorFactory;
  private final TransactionRepository transactionRepository;
  private final MailSender mailSender;

  @Transactional
  public void updateFilledPrices() {
    log.info("OrderUpdater::updateFilledPrices start");

    final var orders = orderRepository.findAllByFilledPriceIsNull();
    final var orderIds = orders
        .stream()
        .map(Order::getStockProduct)
        .map(StockProduct::getId)
        .collect(toSet());
    final var stockProducts = getStockProductsByIds(orderIds);

    orders.forEach(order -> {
      var stockProduct = stockProducts.get(order.getStockProduct().getId());

      if (OrderType.BUY.equals(order.getType())
        && order.getTargetPrice().compareTo(stockProduct.getPrice()) <= 0) {
        processOrder(order, stockProduct, TransactionType.CREDIT);
      } else if (OrderType.SELL.equals(order.getType())
          && order.getTargetPrice().compareTo(stockProduct.getPrice()) >= 0) {
        processOrder(order, stockProduct, TransactionType.DEBIT);
      }
    });

    log.info("OrderUpdater::updateFilledPrices end");
  }

  @Transactional
  private void processOrder(Order order, StockProduct stockProduct, TransactionType transactionType) {
    log.info("OrderUpdater::processOrder start");

    orderValidatorFactory.find(order.getType()).validate(order.getUser().getId(), mapEntityToOrderDto(order));

    order.setFilledPrice(stockProduct.getPrice());
    orderRepository.save(order);

    final var entity = new Transaction();
    entity.setUser(User.builder().id(order.getUser().getId()).build());
    entity.setType(transactionType);
    entity.setAmount(order.getFilledPrice().multiply(BigDecimal.valueOf(order.getAmount())));
    entity.setReason(TransactionReason.ORDER);
    transactionRepository.save(entity);

    log.info("Order: {}, transaction type: {}", order.getId(), transactionType);
    log.info("OrderUpdater::processOrder end");

    generateAndSendOrderNotificationEmail(order);
  }

  private OrderDto mapEntityToOrderDto(Order entity) {
    var dto = new OrderDto();
    dto.setStockProductId(entity.getStockProduct().getId());
    dto.setAmount(entity.getAmount());
    dto.setTargetPrice(entity.getTargetPrice());
    dto.setType(entity.getType());
    return dto;
  }

  private Map<Long, StockProduct> getStockProductsByIds(Set<Long> ids) {
    return stockProductRepository.findAllByIdIn(ids)
        .stream()
        .collect(toMap(StockProduct::getId, Function.identity()));
  }

  private void generateAndSendOrderNotificationEmail(Order order) {
    final var metaData = MailMetaData.builder()
        .subject(MailTemplate.ORDER_NOTIFICATION.getSubject())
        .recipient(order.getUser().getEmail())
        .template(MailTemplate.ORDER_NOTIFICATION)
        .build();

    final var message = new HashMap<String, Object>() {{
      put("orderId", order.getId());
      put("orderType", order.getType());
    }};

    mailSender.send(metaData, message);
  }
}

package az.sctockmarket.api.scheduler;

import az.sctockmarket.api.domain.entities.StockProduct;
import az.sctockmarket.api.repository.StockProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Slf4j
@Component
@RequiredArgsConstructor
public class StockUpdater {

  private final StockProductRepository stockProductRepository;

  @Transactional
  public void updatePrices() {
    log.info("StockUpdater::updatePrices start");

    stockProductRepository.findAll().stream()
        .map(this::changePrice)
        .forEach(stockProductRepository::save);

    log.info("StockUpdater::updatePrices end");
  }

  private StockProduct changePrice(StockProduct stockProduct) {
    var min = BigDecimal.valueOf(0.01);
    var max = stockProduct.getPrice().add(BigDecimal.valueOf(15));
    var newPrice = generateRandomBigDecimalFromRange(min, max);

    log.info("Product: {}, old price: {}, new price: {}",
        stockProduct.getId(),
        stockProduct.getPrice(),
        newPrice);

    stockProduct.setPrice(newPrice);

    return stockProduct;
  }

  private BigDecimal generateRandomBigDecimalFromRange(BigDecimal min, BigDecimal max) {
    return min.add(BigDecimal.valueOf(Math.random()).multiply(max.subtract(min)))
        .setScale(2, RoundingMode.HALF_UP);
  }
}

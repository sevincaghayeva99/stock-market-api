package az.sctockmarket.api.service.strategy.impl;

import az.sctockmarket.api.domain.enums.OrderType;
import az.sctockmarket.api.repository.OrderRepository;
import az.sctockmarket.api.service.strategy.OrderValidatorStrategy;
import az.sctockmarket.api.web.rest.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SellOrderValidatorStrategy implements OrderValidatorStrategy {

  private final OrderRepository orderRepository;

  @Override
  public void validate(Long currentUserId, OrderDto dto) {
    final var inventoryProduct = orderRepository
        .getInventoryProduct(currentUserId, dto.getStockProductId());

    if (inventoryProduct.isEmpty()
        || dto.getAmount().compareTo(inventoryProduct.get().getTotalAmount()) > 0) {
      throw new RuntimeException("Insufficient products");
    }
  }

  @Override
  public OrderType getType() {
    return OrderType.SELL;
  }
}

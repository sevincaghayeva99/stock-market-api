package az.sctockmarket.api.service.strategy.impl;

import az.sctockmarket.api.domain.enums.OrderType;
import az.sctockmarket.api.repository.TransactionRepository;
import az.sctockmarket.api.service.strategy.OrderValidatorStrategy;
import az.sctockmarket.api.web.rest.dto.OrderDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class BuyOrderValidatorStrategy implements OrderValidatorStrategy {

  private final TransactionRepository transactionRepository;

  @Override
  public void validate(Long currentUserId, OrderDto dto) {
    final var orderSum = dto.getTargetPrice().multiply(BigDecimal.valueOf(dto.getAmount()));
    final var currentBalance = transactionRepository.calculateCurrentBalance(currentUserId);

    if (orderSum.compareTo(currentBalance) > 0) {
      throw new RuntimeException("Insufficient funds");
    }
  }

  @Override
  public OrderType getType() {
    return OrderType.BUY;
  }
}

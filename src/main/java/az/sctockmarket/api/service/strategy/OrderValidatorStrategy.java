package az.sctockmarket.api.service.strategy;

import az.sctockmarket.api.domain.enums.OrderType;
import az.sctockmarket.api.web.rest.dto.OrderDto;

public interface OrderValidatorStrategy {

  void validate(Long currentUserId, OrderDto dto);

  OrderType getType();
}

package az.sctockmarket.api.service.strategy;

import az.sctockmarket.api.domain.enums.OrderType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class OrderValidatorFactory {

  private final Map<OrderType, OrderValidatorStrategy> map = new HashMap<>();

  public OrderValidatorFactory(Set<OrderValidatorStrategy> strategies) {
    strategies.forEach(strategy -> map.put(strategy.getType(), strategy));
  }

  public OrderValidatorStrategy find(OrderType type) {
    return map.get(type);
  }
}

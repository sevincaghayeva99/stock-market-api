package az.sctockmarket.api.service;

import az.sctockmarket.api.domain.internal.MailTemplate;

import java.util.Map;

public interface MailContentBuilder {

  String build(MailTemplate template, Map<String, Object> message);
}

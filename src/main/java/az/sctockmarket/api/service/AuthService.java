package az.sctockmarket.api.service;

import az.sctockmarket.api.web.rest.dto.LoginDto;
import az.sctockmarket.api.web.rest.dto.RegisterDto;
import az.sctockmarket.api.web.rest.vm.LoginVm;

public interface AuthService {

  void register(RegisterDto dto);

  LoginVm login(LoginDto dto);

  void verify(String token);
}

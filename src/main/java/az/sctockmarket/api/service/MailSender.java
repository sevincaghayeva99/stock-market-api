package az.sctockmarket.api.service;

import az.sctockmarket.api.domain.internal.MailMetaData;

import java.util.Map;

public interface MailSender {

  void send(MailMetaData metaData, Map<String, Object> message);
}

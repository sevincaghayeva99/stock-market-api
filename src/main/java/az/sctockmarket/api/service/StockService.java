package az.sctockmarket.api.service;

import az.sctockmarket.api.web.rest.vm.StockProductVm;

import java.util.List;

public interface StockService {

  List<StockProductVm> getAll();
}

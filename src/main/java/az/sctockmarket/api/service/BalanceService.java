package az.sctockmarket.api.service;

import az.sctockmarket.api.web.rest.dto.DepositDto;
import az.sctockmarket.api.web.rest.vm.BalanceVm;

public interface BalanceService {

  BalanceVm show();

  void deposit(DepositDto dto);
}

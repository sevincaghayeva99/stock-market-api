package az.sctockmarket.api.service.impl;

import az.sctockmarket.api.domain.entities.StockProduct;
import az.sctockmarket.api.repository.StockProductRepository;
import az.sctockmarket.api.service.StockService;
import az.sctockmarket.api.web.rest.vm.StockProductVm;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {

    private final StockProductRepository stockProductRepository;

    @Override
    @Transactional
    public List<StockProductVm> getAll() {
        return stockProductRepository.findAll().stream().map(this::mapToStockProductVm).toList();
    }

    private StockProductVm mapToStockProductVm(StockProduct entity) {
        var vm = new StockProductVm();
        vm.setId(entity.getId());
        vm.setName(entity.getName());
        vm.setPrice(entity.getPrice());
        return vm;
    }
}

package az.sctockmarket.api.service.impl;

import az.sctockmarket.api.domain.internal.MailTemplate;
import az.sctockmarket.api.service.MailContentBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

import static java.lang.String.join;

@Service
@RequiredArgsConstructor
public class MailContentBuilderImpl implements MailContentBuilder {

  private final TemplateEngine templateEngine;

  @Override
  public String build(MailTemplate template, Map<String, Object> message) {
    final var context = new Context();
    context.setVariable("message", message);
    return templateEngine.process(join(".", template.getFileName(), "html"), context);
  }
}

package az.sctockmarket.api.service.impl;

import az.sctockmarket.api.domain.entities.User;
import az.sctockmarket.api.domain.entities.VerificationToken;
import az.sctockmarket.api.domain.enums.UserStatus;
import az.sctockmarket.api.domain.internal.MailMetaData;
import az.sctockmarket.api.domain.internal.MailTemplate;
import az.sctockmarket.api.repository.UserRepository;
import az.sctockmarket.api.repository.VerificationTokenRepository;
import az.sctockmarket.api.security.JwtUtils;
import az.sctockmarket.api.service.AuthService;
import az.sctockmarket.api.service.MailSender;
import az.sctockmarket.api.web.rest.dto.LoginDto;
import az.sctockmarket.api.web.rest.dto.RegisterDto;
import az.sctockmarket.api.web.rest.vm.LoginVm;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final AuthenticationManager authenticationManager;
  private final UserDetailsService userDetailsService;
  private final JwtUtils jwtUtils;
  private final VerificationTokenRepository verificationTokenRepository;
  private final MailSender mailSender;

  @Override
  @Transactional
  public void register(RegisterDto dto) {
    var user = userRepository.findUserByEmail(dto.getEmail());

    if (user.isPresent()) {
      throw new RuntimeException("Email already used");
    }

    final var createdUser = userRepository.save(mapDtoToUserEntity(dto));

    final var verificationToken = verificationTokenRepository
        .save(createVerificationToken(createdUser.getId()));

    generateAndSendVerificationMail(dto.getEmail(), verificationToken.getUuid());
  }

  @Override
  @Transactional
  public LoginVm login(LoginDto dto) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(dto.getEmail(), dto.getPassword()));

    final var userDetails = userDetailsService.loadUserByUsername(dto.getEmail());

    if (userDetails != null) {
      return new LoginVm(jwtUtils.generateToken(userDetails));
    }

    throw new UsernameNotFoundException("No user was found");
  }

  @Override
  @Transactional
  public void verify(String token) {
    verificationTokenRepository.findById(token)
        .ifPresentOrElse(
            verificationToken -> {

              final var user = userRepository
                  .findById(verificationToken.getUser().getId())
                  .orElseThrow(() -> new RuntimeException("No such user"));
              user.setStatus(UserStatus.ACTIVE);
              userRepository.save(user);

              verificationTokenRepository.delete(verificationToken);
            }, () -> {
              throw new RuntimeException("No such token");
            }
        );
  }

  private User mapDtoToUserEntity(RegisterDto dto) {
    var user = new User();
    user.setFullName(dto.getFullName());
    user.setEmail(dto.getEmail());
    user.setPassword(passwordEncoder.encode(dto.getPassword()));
    user.setStatus(UserStatus.REGISTERED);
    return user;
  }

  private VerificationToken createVerificationToken(Long userId) {
    var entity = new VerificationToken();
    entity.setUser(User.builder().id(userId).build());
    return entity;
  }

  private void generateAndSendVerificationMail(String email,String token) {
    final var metaData = MailMetaData.builder()
            .subject(MailTemplate.REGISTER_VERIFICATION.getSubject())
            .recipient(email)
            .template(MailTemplate.REGISTER_VERIFICATION)
            .build();

    final var message = new HashMap<String, Object>() {{
      put("token", token);
    }};

    mailSender.send(metaData, message);
  }
}

package az.sctockmarket.api.service.impl;

import az.sctockmarket.api.domain.entities.Order;
import az.sctockmarket.api.domain.entities.StockProduct;
import az.sctockmarket.api.domain.entities.User;
import az.sctockmarket.api.repository.OrderRepository;
import az.sctockmarket.api.repository.entity.InventoryProduct;
import az.sctockmarket.api.security.CurrentUserUtils;
import az.sctockmarket.api.service.OrderService;
import az.sctockmarket.api.service.strategy.OrderValidatorFactory;
import az.sctockmarket.api.web.rest.dto.OrderDto;
import az.sctockmarket.api.web.rest.vm.InventoryProductVm;
import az.sctockmarket.api.web.rest.vm.OrderVm;
import az.sctockmarket.api.web.rest.vm.ShowOrderVm;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final OrderValidatorFactory orderValidatorFactory;
  private final CurrentUserUtils currentUserUtils;
  private final OrderRepository orderRepository;

  @Override
  @Transactional
  public List<InventoryProductVm> showInventory() {
    final var currentUserId = currentUserUtils.getId();

    return orderRepository.getInventory(currentUserId)
        .stream()
        .map(this::mapToInventoryProductVm)
        .toList();
  }

  @Override
  @Transactional
  public List<ShowOrderVm> showOrders() {
    final var currentUserId = currentUserUtils.getId();

    return orderRepository.findAllByUserId(currentUserId)
        .stream()
        .map(this::mapToShowOrderVm)
        .toList();
  }

  @Override
  @Transactional
  public OrderVm order(OrderDto dto) {
    final var currentUserId = currentUserUtils.getId();

    orderValidatorFactory.find(dto.getType()).validate(currentUserId, dto);

    final var saved = orderRepository.save(mapDtoToOrderEntity(currentUserId, dto));
    return new OrderVm(saved.getId());
  }

  private InventoryProductVm mapToInventoryProductVm(InventoryProduct entity) {
    final var vm = new InventoryProductVm();
    vm.setId(entity.getId());
    vm.setName(entity.getName());
    vm.setTotalAmount(entity.getTotalAmount());
    vm.setTotalFilledPrice(entity.getTotalFilledPrice());
    return vm;
  }

  private ShowOrderVm mapToShowOrderVm(Order entity) {
    final var vm = new ShowOrderVm();
    vm.setId(entity.getId());
    vm.setUser(mapToUserVm(entity.getUser()));
    vm.setStockProduct(mapToStockProductVm(entity.getStockProduct()));
    vm.setAmount(entity.getAmount());
    vm.setTargetPrice(entity.getTargetPrice());
    vm.setFilledPrice(entity.getFilledPrice());
    vm.setType(entity.getType());
    vm.setCreateTs(entity.getCreateTs());
    vm.setUpdateTs(entity.getUpdateTs());
    return vm;
  }

  private ShowOrderVm.UserVm mapToUserVm(User entity) {
    final var vm = new ShowOrderVm.UserVm();
    vm.setId(entity.getId());
    vm.setFullName(entity.getFullName());
    vm.setEmail(entity.getEmail());
    vm.setRegisterTs(entity.getRegisterTs());
    return vm;
  }

  private ShowOrderVm.StockProductVm mapToStockProductVm(StockProduct entity) {
    final var vm = new ShowOrderVm.StockProductVm();
    vm.setId(entity.getId());
    vm.setName(entity.getName());
    return vm;
  }

  private Order mapDtoToOrderEntity(Long currentUserId, OrderDto dto) {
    final var order = new Order();
    order.setUser(User.builder().id(currentUserId).build());
    order.setStockProduct(StockProduct.builder().id(dto.getStockProductId()).build());
    order.setAmount(dto.getAmount());
    order.setTargetPrice(dto.getTargetPrice());
    order.setType(dto.getType());
    return order;
  }
}

package az.sctockmarket.api.service.impl;

import az.sctockmarket.api.config.properties.ApplicationProperties;
import az.sctockmarket.api.domain.internal.MailMetaData;
import az.sctockmarket.api.service.MailContentBuilder;
import az.sctockmarket.api.service.MailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailSenderImpl implements MailSender {

  private final ApplicationProperties applicationProperties;
  private final JavaMailSender javaMailSender;
  private final MailContentBuilder mailContentBuilder;

  @Override
  public void send(MailMetaData metaData, Map<String, Object> message) {
    final MimeMessagePreparator messagePreparator = mimeMessage -> {
      final var messageHelper = new MimeMessageHelper(mimeMessage);
      messageHelper.setFrom(applicationProperties.getEmail().getNoReply());
      messageHelper.setTo(metaData.getRecipient());
      messageHelper.setSubject(metaData.getSubject());
      messageHelper.setText(mailContentBuilder.build(metaData.getTemplate(), message));
    };

    try {
      javaMailSender.send(messagePreparator);
    } catch (MailException ex) {
      throw new RuntimeException("Exception occurred when sending mail to " + metaData.getRecipient());
    }
  }
}

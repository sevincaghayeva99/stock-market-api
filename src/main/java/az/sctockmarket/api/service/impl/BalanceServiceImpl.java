package az.sctockmarket.api.service.impl;

import az.sctockmarket.api.domain.entities.Transaction;
import az.sctockmarket.api.domain.entities.User;
import az.sctockmarket.api.domain.enums.TransactionReason;
import az.sctockmarket.api.domain.enums.TransactionType;
import az.sctockmarket.api.repository.TransactionRepository;
import az.sctockmarket.api.security.CurrentUserUtils;
import az.sctockmarket.api.service.BalanceService;
import az.sctockmarket.api.web.rest.dto.DepositDto;
import az.sctockmarket.api.web.rest.vm.BalanceVm;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BalanceServiceImpl implements BalanceService {

  private final CurrentUserUtils currentUserUtils;
  private final TransactionRepository transactionRepository;

  @Override
  @Transactional
  public BalanceVm show() {
    final var currentUserId = currentUserUtils.getId();

    final var currentBalance = transactionRepository.calculateCurrentBalance(currentUserId);
    return new BalanceVm(currentBalance);
  }

  @Override
  @Transactional
  public void deposit(DepositDto dto) {
    final var currentUserId = currentUserUtils.getId();

    var transaction = mapDtoToTransactionEntity(currentUserId, dto);
    transactionRepository.save(transaction);
  }

  private Transaction mapDtoToTransactionEntity(Long currentUserId, DepositDto dto) {
    final var transaction = new Transaction();
    transaction.setUser(User.builder().id(currentUserId).build());
    transaction.setType(TransactionType.DEBIT);
    transaction.setAmount(dto.getAmount());
    transaction.setReason(TransactionReason.DEPOSIT);
    return transaction;
  }
}

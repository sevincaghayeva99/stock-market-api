package az.sctockmarket.api.service;

import az.sctockmarket.api.web.rest.dto.OrderDto;
import az.sctockmarket.api.web.rest.vm.InventoryProductVm;
import az.sctockmarket.api.web.rest.vm.OrderVm;
import az.sctockmarket.api.web.rest.vm.ShowOrderVm;

import java.util.List;

public interface OrderService {

  List<InventoryProductVm> showInventory();

  List<ShowOrderVm> showOrders();

  OrderVm order(OrderDto dto);
}

package az.sctockmarket.api.domain.enums;

public enum UserStatus {

  REGISTERED, ACTIVE
}

package az.sctockmarket.api.domain.enums;

public enum TransactionReason {

  DEPOSIT, ORDER
}

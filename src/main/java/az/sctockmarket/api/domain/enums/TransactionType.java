package az.sctockmarket.api.domain.enums;

public enum TransactionType {

  DEBIT, CREDIT
}

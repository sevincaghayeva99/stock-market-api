package az.sctockmarket.api.domain.enums;

public enum OrderType {

  BUY, SELL
}

package az.sctockmarket.api.domain.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailMetaData {

  private String subject;
  private String recipient;
  private MailTemplate template;
}

package az.sctockmarket.api.domain.internal;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MailTemplate {
  REGISTER_VERIFICATION("register-verification-mail-template", "Please verify your email address"),
  ORDER_NOTIFICATION("order-notification-mail-template", "Stock Order notification");

  private final String fileName;
  private final String subject;
}

package az.sctockmarket.api.domain.entities;

import az.sctockmarket.api.domain.enums.UserStatus;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.OffsetDateTime;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String fullName;
  private String email;

  @ToString.Exclude
  private String password;

  @Enumerated(EnumType.STRING)
  private UserStatus status;

  @CreationTimestamp
  private OffsetDateTime registerTs;
}

package az.sctockmarket.api.domain.entities;

import az.sctockmarket.api.domain.enums.TransactionReason;
import az.sctockmarket.api.domain.enums.TransactionType;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "transactions")
public class Transaction {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private String uuid;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private User user;

  @Enumerated(EnumType.STRING)
  private TransactionType type;

  private BigDecimal amount;

  @Enumerated(EnumType.STRING)
  private TransactionReason reason;

  @CreationTimestamp
  private OffsetDateTime transactionTs;
}

package az.sctockmarket.api.domain.entities;

import az.sctockmarket.api.domain.enums.OrderType;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "orders")
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  private User user;

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  private StockProduct stockProduct;

  private Integer amount;

  private BigDecimal targetPrice;

  private BigDecimal filledPrice;

  @Enumerated(EnumType.STRING)
  private OrderType type;

  @CreationTimestamp
  private OffsetDateTime createTs;

  @UpdateTimestamp
  private OffsetDateTime updateTs;
}

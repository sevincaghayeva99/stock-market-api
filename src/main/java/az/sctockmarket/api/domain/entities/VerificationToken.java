package az.sctockmarket.api.domain.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "verification_tokens")
public class VerificationToken {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private String uuid;

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  private User user;

  @CreationTimestamp
  private OffsetDateTime createTs;

}

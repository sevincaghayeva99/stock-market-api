package az.sctockmarket.api.config.properties;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationPropertiesScan
@ConfigurationProperties(prefix = "stock-market-api")
public class ApplicationProperties {

  @NotNull
  private JwtProperties jwt;

  @NotNull
  private EmailProperties email;
}

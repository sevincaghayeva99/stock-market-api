package az.sctockmarket.api.repository.entity;

import java.math.BigDecimal;

public interface InventoryProduct {

  Long getId();
  String getName();
  Integer getTotalAmount();
  BigDecimal getTotalFilledPrice();
}

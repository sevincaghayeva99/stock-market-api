package az.sctockmarket.api.repository;

import az.sctockmarket.api.domain.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, String> {

  @Query(value = """
    SELECT COALESCE(
                    SUM(COALESCE(CASE WHEN type = 'DEBIT' THEN amount END, 0))
                    - SUM(COALESCE(CASE WHEN type = 'CREDIT' THEN amount END, 0)), 0) AS balance
    FROM transactions
    WHERE user_id = :userId
    """, nativeQuery = true)
  BigDecimal calculateCurrentBalance(@Param("userId") Long userId);
}

package az.sctockmarket.api.repository;

import az.sctockmarket.api.domain.entities.Order;
import az.sctockmarket.api.repository.entity.InventoryProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

  List<Order> findAllByUserId(Long userId);

  Set<Order> findAllByFilledPriceIsNull();

  @Query(value = """
    SELECT sp.id AS id,
           sp.name AS name,
           SUM(COALESCE(CASE WHEN o.type = 'BUY' THEN o.amount END, 0))
           - SUM(COALESCE(CASE WHEN o.type = 'SELL' THEN o.amount END, 0)) AS totalAmount,
           SUM(COALESCE(CASE WHEN o.type = 'BUY' THEN o.filled_price END, 0))
           - SUM(COALESCE(CASE WHEN o.type = 'SELL' THEN o.filled_price END, 0)) AS totalFilledPrice
    FROM orders o
             LEFT JOIN stock_products sp ON o.stock_product_id = sp.id
    WHERE o.user_id = :userId
      AND o.filled_price IS NOT NULL
    GROUP BY o.stock_product_id
    """, nativeQuery = true)
  Set<InventoryProduct> getInventory(@Param("userId") Long userId);

  @Query(value = """
    SELECT sp.id AS id,
           sp.name AS name,
           SUM(COALESCE(CASE WHEN o.type = 'BUY' THEN o.amount END, 0))
           - SUM(COALESCE(CASE WHEN o.type = 'SELL' THEN o.amount END, 0)) AS totalAmount,
           SUM(COALESCE(CASE WHEN o.type = 'BUY' THEN o.filled_price END, 0))
           - SUM(COALESCE(CASE WHEN o.type = 'SELL' THEN o.filled_price END, 0)) AS totalFilledPrice
    FROM orders o
             LEFT JOIN stock_products sp ON o.stock_product_id = sp.id
    WHERE o.user_id = :userId
      AND o.filled_price IS NOT NULL
      AND o.stock_product_id = :productId
    GROUP BY o.stock_product_id
    """, nativeQuery = true)
  Optional<InventoryProduct> getInventoryProduct(@Param("userId") Long userId,
                                                 @Param("productId") Long productId);
}

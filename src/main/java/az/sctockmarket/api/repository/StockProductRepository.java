package az.sctockmarket.api.repository;

import az.sctockmarket.api.domain.entities.StockProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface StockProductRepository extends JpaRepository<StockProduct, Long> {

  Set<StockProduct> findAllByIdIn(Set<Long> ids);
}

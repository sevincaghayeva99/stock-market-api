package az.sctockmarket.api.security;

import az.sctockmarket.api.domain.enums.UserStatus;
import az.sctockmarket.api.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static java.util.Collections.singletonList;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    final var user = userRepository.findUserByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException("No user was found"));

    return SecurityUser.builder()
        .id(user.getId())
        .username(user.getEmail())
        .password(user.getPassword())
        .authorities(getAuthorities("ROLE_ORG"))
        .isAccountNonExpired(true)
        .isAccountNonLocked(true)
        .isCredentialsNonExpired(true)
        .isEnabled(UserStatus.ACTIVE.equals(user.getStatus()))
        .build();
  }

  private Collection<? extends GrantedAuthority> getAuthorities(String authority) {
    return singletonList(new SimpleGrantedAuthority(authority));
  }
}

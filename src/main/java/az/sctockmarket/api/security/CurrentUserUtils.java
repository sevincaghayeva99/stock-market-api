package az.sctockmarket.api.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserUtils {

  public SecurityUser get() {
    return (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  public Long getId() {
    return get().getId();
  }

  public String getEmail() {
    return get().getUsername();
  }
}

package az.sctockmarket.api.web.rest;

import az.sctockmarket.api.web.rest.dto.LoginDto;
import az.sctockmarket.api.web.rest.dto.RegisterDto;
import az.sctockmarket.api.web.rest.vm.LoginVm;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface AuthController {

  @PostMapping("/register")
  ResponseEntity<Void> register(@RequestBody @Validated RegisterDto dto);

  @PostMapping("/login")
  ResponseEntity<LoginVm> login(@RequestBody @Validated LoginDto dto);

  @GetMapping("/verify")
  ResponseEntity<Void> verify(@RequestParam("token") String token);
}

package az.sctockmarket.api.web.rest.impl;

import az.sctockmarket.api.service.AuthService;
import az.sctockmarket.api.web.rest.AuthController;
import az.sctockmarket.api.web.rest.dto.LoginDto;
import az.sctockmarket.api.web.rest.dto.RegisterDto;
import az.sctockmarket.api.web.rest.vm.LoginVm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthControllerImpl implements AuthController {

  private final AuthService authService;

  @Override
  public ResponseEntity<Void> register(RegisterDto dto) {
    authService.register(dto);
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<LoginVm> login(LoginDto dto) {
    return ResponseEntity.ok(authService.login(dto));
  }

  @Override
  public ResponseEntity<Void> verify(String token) {
    authService.verify(token);
    return ResponseEntity.ok().build();
  }
}

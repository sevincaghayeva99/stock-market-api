package az.sctockmarket.api.web.rest.impl;

import az.sctockmarket.api.service.OrderService;
import az.sctockmarket.api.web.rest.OrderController;
import az.sctockmarket.api.web.rest.dto.OrderDto;
import az.sctockmarket.api.web.rest.vm.InventoryProductVm;
import az.sctockmarket.api.web.rest.vm.OrderVm;
import az.sctockmarket.api.web.rest.vm.ShowOrderVm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/order")
public class OrderControllerImpl implements OrderController {

  private final OrderService orderService;

  @Override
  public ResponseEntity<List<InventoryProductVm>> showInventory() {
    return ResponseEntity.ok(orderService.showInventory());
  }

  @Override
  public ResponseEntity<List<ShowOrderVm>> showOrders() {
    return ResponseEntity.ok(orderService.showOrders());
  }

  @Override
  public ResponseEntity<OrderVm> order(OrderDto dto) {
    return ResponseEntity.status(HttpStatus.CREATED).body(orderService.order(dto));
  }
}

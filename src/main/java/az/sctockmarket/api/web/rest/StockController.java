package az.sctockmarket.api.web.rest;

import az.sctockmarket.api.web.rest.vm.StockProductVm;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface StockController {

  @GetMapping
  ResponseEntity<List<StockProductVm>> getAll();
}

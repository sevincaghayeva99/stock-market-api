package az.sctockmarket.api.web.rest;

import az.sctockmarket.api.web.rest.dto.DepositDto;
import az.sctockmarket.api.web.rest.vm.BalanceVm;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface BalanceController {

  @GetMapping
  ResponseEntity<BalanceVm> show();

  @PostMapping("/deposit")
  ResponseEntity<Void> deposit(@RequestBody @Validated DepositDto dto);
}

package az.sctockmarket.api.web.rest.impl;

import az.sctockmarket.api.service.StockService;
import az.sctockmarket.api.web.rest.StockController;
import az.sctockmarket.api.web.rest.vm.StockProductVm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/stock")
public class StockControllerImpl implements StockController {

    private final StockService stockService;

    @Override
    public ResponseEntity<List<StockProductVm>> getAll() {
        return ResponseEntity.ok(stockService.getAll());
    }
}

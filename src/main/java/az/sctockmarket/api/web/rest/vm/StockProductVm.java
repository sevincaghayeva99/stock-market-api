package az.sctockmarket.api.web.rest.vm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockProductVm {

  private Long id;
  private String name;
  private BigDecimal price;
}

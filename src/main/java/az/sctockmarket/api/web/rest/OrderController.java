package az.sctockmarket.api.web.rest;

import az.sctockmarket.api.web.rest.dto.OrderDto;
import az.sctockmarket.api.web.rest.vm.InventoryProductVm;
import az.sctockmarket.api.web.rest.vm.OrderVm;
import az.sctockmarket.api.web.rest.vm.ShowOrderVm;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface OrderController {

  @GetMapping("/inventory")
  ResponseEntity<List<InventoryProductVm>> showInventory();

  @GetMapping
  ResponseEntity<List<ShowOrderVm>> showOrders();

  @PostMapping
  ResponseEntity<OrderVm> order(@RequestBody @Validated OrderDto dto);
}

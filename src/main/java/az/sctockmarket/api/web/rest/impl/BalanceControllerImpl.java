package az.sctockmarket.api.web.rest.impl;

import az.sctockmarket.api.service.BalanceService;
import az.sctockmarket.api.web.rest.BalanceController;
import az.sctockmarket.api.web.rest.dto.DepositDto;
import az.sctockmarket.api.web.rest.vm.BalanceVm;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/balance")
public class BalanceControllerImpl implements BalanceController {

  private final BalanceService balanceService;

  @Override
  public ResponseEntity<BalanceVm> show() {
    return ResponseEntity.ok(balanceService.show());
  }

  @Override
  public ResponseEntity<Void> deposit(DepositDto dto) {
    balanceService.deposit(dto);
    return ResponseEntity.ok().build();
  }
}

package az.sctockmarket.api.web.rest.dto;

import az.sctockmarket.api.domain.enums.OrderType;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

  @Min(0)
  @NotNull
  private Long stockProductId;

  @Min(0)
  @NotNull
  private Integer amount;

  @Min(0)
  @NotNull
  private BigDecimal targetPrice;

  @NotNull
  private OrderType type;
}

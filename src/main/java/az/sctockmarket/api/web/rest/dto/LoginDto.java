package az.sctockmarket.api.web.rest.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginDto {

  @Email
  @NotBlank
  @Size(max = 256)
  private String email;

  @NotBlank
  @ToString.Exclude
  @Size(min = 6, max = 16)
  private String password;
}

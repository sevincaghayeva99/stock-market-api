package az.sctockmarket.api.web.rest.vm;

import az.sctockmarket.api.domain.enums.OrderType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShowOrderVm {

  private Long id;
  private UserVm user;
  private StockProductVm stockProduct;
  private Integer amount;
  private BigDecimal targetPrice;
  private BigDecimal filledPrice;
  private OrderType type;
  private OffsetDateTime createTs;
  private OffsetDateTime updateTs;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class UserVm {
    private Long id;
    private String fullName;
    private String email;
    private OffsetDateTime registerTs;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class StockProductVm {
    private Long id;
    private String name;
  }
}
